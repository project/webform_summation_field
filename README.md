# Webform Summation Field

This module will provide a new field type called "summation field", admin can collect the values of the webform fields. The field will hide in the webform.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/webform_summation_field).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/webform_summation_field).

## Requirements

This module requires the following modules:
- [Webform](https://www.drupal.org/project/webform)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Maintainers

- aloneblood - [aloneblood](https://www.drupal.org/u/aloneblood)
- Li Qing - [li-qing](https://www.drupal.org/u/li-qing)
- Simba Wang - [simbaw](https://www.drupal.org/u/simbaw)